import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  FlatList,
} from "react-native";
import { TextInput as PaperTextInput , Button } from "react-native-paper";
import MCIcon from "react-native-vector-icons/MaterialCommunityIcons";

export default function App() {
  const [listStudent, setListStudent] = useState([]);
  const [student, setStudent] = useState({ name: "", age: "", isCheck: false });

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />

      {/* Add a New Student */}
      <View style={styles.section}>
        <Text>Add New Student</Text>
        <PaperTextInput
          value={student.name}
          onChangeText={(text) => {
            const newStudent = { ...student, name: text };
            setStudent(newStudent);
          }}
          label="Name"
          style={styles.input}
          mode="outlined"
        />
        <PaperTextInput
          value={student.age}
          label="Age"
          style={styles.input}
          onChangeText={(text) => {
            const newStudent = { ...student, age: text };
            setStudent(newStudent);
          }}
          mode="outlined"
        />
        {/* <TouchableOpacity
          style={styles.button}
          onPress={() => {
            const studentList = [...listStudent, student];
            setListStudent(studentList);
          }}
        >
          <Text>Add</Text>
        </TouchableOpacity> */}
        <Button
          icon="account-plus"
          mode="contained"
          onPress={() => {
            const studentList = [...listStudent, student];
            setListStudent(studentList);
          }}
        >
          Add
        </Button>
      </View>

      {/* Display List of Student */}
      <View style={styles.section}>
        <View style={{ flexDirection: "row" }}>
          <MCIcon
            name="format-list-bulleted"
            style={{ fontSize: 24, color: "blue" }}
          />
          <Text>List of Students</Text>
        </View>
        <View style={{ borderWidth: 1, height: 300, paddingHorizontal: 20 }}>
          <FlatList
            data={listStudent}
            renderItem={(data) => {
              const student = data.item;
              const studentIndex = data.index;
              return (
                <TouchableOpacity
                  style={[
                    styles.listItem,
                    { backgroundColor: student.isCheck ? "yellow" : "#ccc" },
                  ]}
                  onPress={() => {
                    const newStudent = { ...listStudent[studentIndex] };
                    newStudent.isCheck = !newStudent.isCheck;

                    const studentList = [...listStudent];
                    studentList[studentIndex] = newStudent;

                    setListStudent(studentList);
                  }}
                >
                  <Text>Name: {student.name}</Text>
                  <Text>Age: {student.age}</Text>
                </TouchableOpacity>
              );
            }}
            keyExtractor={(data, index) => {
              return index.toString();
            }}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    // justifyContent: "center",
  },
  input: {
    width: 200,
    // height: 40,
    // borderWidth: 1,
    // padding: 10,
    margin: 5,
  },
  button: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ccc",
    borderRadius: 5,
  },
  section: {
    marginTop: 50,
  },
  listItem: {
    padding: 10,
    backgroundColor: "#ccc",
    borderRadius: 10,
    margin: 5,
    // flexDirection: "row"
  },
});
